package com.jijs.jetty.example;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;

/**
 * 这个demo里面的handler为ContextHandler，同样，他也是handlerWrapper的一个子类
 * 
 * 这里实现的是，在这个contextHandler里面再设置handler，将转发给先一个handler（这里为helloHandler）
 * 
 * @author cc
 *
 */
public class Jetty3 {

	public static void main(String[] args) {
		Server server = new Server(8080); // server
		
		ContextHandler context = new ContextHandler(); // handler
		context.setContextPath("/hello");
		// context.setClassLoader(Thread.currentThread().getContextClassLoader());
		context.setHandler(new HelloHandler());
		
		ContextHandler context2 = new ContextHandler(); // handler
		context2.setContextPath("/hello2");
		// context.setClassLoader(Thread.currentThread().getContextClassLoader());
		context2.setHandler(new HelloHandler());
		
		
		ResourceHandler handler = new ResourceHandler();// 静态资源处理的handler
		handler.setDirectoriesListed(false);// 是否显示文件列表
		handler.setWelcomeFiles(new String[] { "index.html" });
		handler.setResourceBase("src/main/webapp");
		
		HandlerCollection list = new HandlerCollection();
		list.addHandler(handler);
		list.addHandler(context);
		list.addHandler(context2);
		server.setHandler(list);
		
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// HelloHandler类：
	public static class HelloHandler extends AbstractHandler {
		public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {
			response.setContentType("text/html;charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			baseRequest.setHandled(true);
			response.getWriter().write("hello world");
			response.getWriter().write("Request url：" + baseRequest);
		}
	}
}

/**
 * 这里的ClassLoader为类加载器，作用就是讲所有的东东加载到虚拟机，如果这里不设置的话，那么会创建一个默认的 ClassLoader。
 * 
 * 访问路径：http://localhost:8080/hello..... 将输出HelloHandler里面的hello world和请求的地址
 */
