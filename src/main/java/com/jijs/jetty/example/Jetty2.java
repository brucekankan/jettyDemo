package com.jijs.jetty.example;

import java.io.IOException;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

//import cn.com.infcn.monitor.filter.MonitoringFilter;

/**
 * 这个Demo阐述了jetty与servlet之间的事情，他们之间的连接就是servletContextHandler，
 * 这是handlerWrapper的一个子类，这个在jetty的嵌入式使用(1)中已经阐述过了。
 * 
 * servletContextHandler添加servlet采用的是addServlet的方式，里面需要实例ServletHolder（
 * 这里为HelloHandler）以及访问的路径，可以添加多个servlet进行处理
 * 
 * @author cc
 *
 */
public class Jetty2 {

	public static void main(String[] args) throws Exception {
		Server server = new Server(8082);
		System.out.println(ServletContextHandler.SESSIONS); // 1
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/*");
//		context.addServlet(new ServletHolder(new HelloServlet()), "/ifcmonitor/index.html");
//		context.addServlet(new ServletHolder(new HelloServlet("上学")), "/school");
//		context.addServlet(new ServletHolder(new HelloServlet("小明")), "/person");
//		context.addFilter(new FilterHolder(new MyFilter()), "/*", EnumSet.of(DispatcherType.REQUEST));
//		context.addFilter(new FilterHolder(new MonitoringFilter()), "/*", EnumSet.of(DispatcherType.REQUEST));
		ResourceHandler handler = new ResourceHandler();// 静态资源处理的handler
		handler.setDirectoriesListed(true);// 是否显示文件列表
		handler.setWelcomeFiles(new String[] { "index.html" });
		handler.setResourceBase("src/main/webapp");
		
//		CrossOriginFilter filter=new CrossOriginFilter();
//		  FilterHolder filterHolder=new FilterHolder(filter);
//		  context.addFilter(filterHolder,cometServletPath + "/*",FilterMapping.REQUEST);
		// context.addFilter(MonitoringFilter.class, "/*",
		// EnumSet.of(DispatcherType.REQUEST));
		HandlerCollection list = new HandlerCollection();
		list.addHandler(handler);
		list.addHandler(context);
		
		server.setHandler(list);
		server.start();
		server.join();
	}

	// HelloServlet类：
	public static class HelloServlet extends HttpServlet {
		String greeting = "哇哈哈";

		public HelloServlet() {
		}

		public HelloServlet(String greeting) {
			this.greeting = greeting;
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			this.doPost(request, response);
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().write("<h1>" + greeting + "</h1>");
			response.getWriter().write("session= " + request.getSession(true).getId());
		}

	}
	
	public static class MyFilter implements Filter{

		@Override
		public void destroy() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
				throws IOException, ServletException {
			HttpServletRequest request = (HttpServletRequest)arg0;
			HttpServletResponse response = (HttpServletResponse)arg1;
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().write("<h1> 123456 </h1>");
			response.getWriter().write("session= " + request.getSession(true).getId());
		}

		@Override
		public void init(FilterConfig arg0) throws ServletException {
			System.out.println("nihao a ");
		}

		
	}
}
/**
 * 由于servletContextHandler设置了contextPath路径为/content所以，访问路径为：
 * 
 * http://localhost:8082/content.... 打印HelloServlet的默认显示 哇哈哈
 * 
 * http://localhost:8082/content/school 显示上学
 * 
 * http://localhost:8082/content/person 显示小明
 * 
 */
