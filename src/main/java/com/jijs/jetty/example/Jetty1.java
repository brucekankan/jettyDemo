package com.jijs.jetty.example;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;

/**
 * 这个Demo展现了jetty对静态资源的访问，Server的创建可以设置端口号，即使没设置，也可以通过Connector设置，
 * 这里使用的是nio方式的SelectChannelConnector，之所以使用nio，是因为nio可以实现非阻塞。
 * 
 * connector和handler不是必须的，也可以是多个，这里体现了server的灵活性，需要什么就添加什么。
 * 
 * @author cc
 *
 */
public class Jetty1 {

	public static void main(String[] args) throws Exception {
		Server server = new Server(8080);
		// SelectChannelConnector connector = new SelectChannelConnector();
		// connector.setPort(8080);
		// server.addConnector(connector);

		ResourceHandler handler = new ResourceHandler();// 静态资源处理的handler
		handler.setDirectoriesListed(false);// 是否显示文件列表
		handler.setWelcomeFiles(new String[] { "index.html" });
		handler.setResourceBase("src/main/webapp");
		server.setHandler(handler);
		server.start();
		server.join();
	}
}

/**
 * 1.ResourceHandler：接对handle接口进行实现
 * 
 * 2.静态资源放在那里，resourceBase就要设置为响应的目录，因为静态资源就放在里面，这里我的放在根目录下，所以为WebContent
 * 
 * 3.访问路径 http://localhost:8080 ，可以访问到index1.html里面的内容
 */
