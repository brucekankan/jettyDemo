package com.jijs.jetty.example;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;

import com.jijs.jetty.example.Jetty3.HelloHandler;

/**
 * 这个demo实现了对server作为服务器，监听多个端口发来的请求，很简单的实现， 只是创建了多个connector就能达到目的。
 * 
 * @author cc
 *
 */
public class Jetty6 {

	/**
	 * 访问路径：http://localhost:8082/..... 端口为8082的请求将被发送到helloHandler里面处理
	 * 
	 * http://localhost:9001/..... 端口为9001的请求将被发送到helloHandler里面处理
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Server server = new Server();
		SelectChannelConnector connector1 = new SelectChannelConnector();
		connector1.setPort(8082);
		SelectChannelConnector connector2 = new SelectChannelConnector();
		connector2.setPort(9001);
		server.addConnector(connector1);
		server.addConnector(connector2);
		server.setHandler(new HelloHandler());
		server.start();
		server.join();
	}
}
