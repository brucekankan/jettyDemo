package com.jijs.jetty.example;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.jijs.jetty.example.Jetty3.HelloHandler;

/**
 * 在这个demo中，server部署了两个handler类，分别为ServletContextHandler和WebAppContext
 * 但要同时部署这两个handler类的话，就必须使用HandlerList 或者HandlerCollection
 * ，否则的话，如果两次都调用setHandler，那么前一次将会被后一次覆盖。
 * 
 * @author cc
 *
 */
public class Jetty5 {

	public static void main(String[] args) throws Exception {
		Server server = new Server(8082);
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/context");
		context.addServlet(new ServletHolder(new HelloServlet()), "/*");
		context.addServlet(new ServletHolder(new HelloServlet("上学")), "/school");
		context.addServlet(new ServletHolder(new HelloServlet("小明")), "/person");

		ContextHandler context1 = new ContextHandler(); // handler
		context1.setContextPath("/hello");
		// context.setClassLoader(Thread.currentThread().getContextClassLoader());
		context1.setHandler(new HelloHandler());

		// WebAppContext appContext = new WebAppContext();
		// appContext.setContextPath("/appContext");
		// appContext.setWar("d:/test/TestJetty.war");

		ResourceHandler handler = new ResourceHandler();// 静态资源处理的handler
		handler.setDirectoriesListed(false);// 是否显示文件列表
		handler.setWelcomeFiles(new String[] { "index.html" });
		handler.setResourceBase("src/main/webapp");
		HandlerCollection handlers = new HandlerCollection();
		// HandlerList handlers = new HandlerList();
		// handlers.setHandlers(new Handler[] { context, context1, handler });
		handlers.addHandler(context);
		handlers.addHandler(context1);
		handlers.addHandler(handler);
		server.setHandler(handlers);
		server.start();
		server.join();
	}

	/**
	 * 这个demo，实质上就是将ServletContextHandler 和WebAppContext 两块的内容放在一起了。 访问路径： 1,输入
	 * http://localhost:8082/context/....将打印helloServlet的默认语句 2.输入
	 * http://localhost:8082/context/school 将打印上学 3.输入
	 * http://localhost:8082/context/person 将打印小明 4.输入
	 * http://localhost:8082/appContext/.... 将打印TestJetty下面的相关东东
	 * 
	 * 但是HandlerList 和HandlerCollection还是有区别的：
	 * HandlerList和HandlerConnection内部都可以设置若干Handler，
	 * Handler按顺序一个接一个的执行。对于HandlerList而言，只要有一个Handler将请求标记为已处理，或抛出异常，
	 * Handler的调用就到此结束。而HandlerConnection则不会结束，一直调用到最后一个Handler。
	 *
	 */

	// HelloServlet 类：
	public static class HelloServlet extends HttpServlet {
		String greeting = "哇哈哈";

		public HelloServlet() {
		}

		public HelloServlet(String greeting) {
			this.greeting = greeting;
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			this.doPost(request, response);
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().write("<h1>" + greeting + "</h1>");
			response.getWriter().write("session= " + request.getSession(true).getId());
		}

	}
}
