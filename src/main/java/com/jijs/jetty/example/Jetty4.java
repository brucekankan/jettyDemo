package com.jijs.jetty.example;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * 这个demo使用的handler是WebAppContext，它是contextHandler的子类，
 * 
 * contextHandler可以调用方法setHandler， 但是此处用的WebAppContext和上文用的ServletContextHandler
 * 
 * 如果调用了setHandler的话，都将报错：java.lang.IllegalStateException: !ScopedHandler
 * 
 * @author cc
 *
 */
public class Jetty4 {

	public static void main(String[] args) throws Exception {
		Server server = new Server(8080);
		WebAppContext context = new WebAppContext();
		context.setContextPath("/hello");
		// context.setClassLoader(Thread.currentThread().getContextClassLoader());
		context.setWar("d:/test/TestJetty.war");
		server.setHandler(context);
		server.start();
		server.join();
	}
}

/**
 * 如根目录下有index.jsp文件，test文件夹里面有test.jsp文件
 * 则访问路径为：http://localhost:8080/hello/index.jsp
 * http://localhost:8080/hello/test/ 将列出test文件夹下面的所有目录结构
 * http://localhost:8080/hello/test/test.jsp
 */
